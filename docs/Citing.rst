.. _citing:

Citing GX
+++++++++++++++

If you use GX in your work, please cite the following papers:

N. R. Mandell, W. D. Dorland, and M. Landreman. 2018. "Laguerre-Hermite pseudo-spectral velocity formulation of gyrokinetics". J. Plasma Phys. **84**, 905840108. `<https://doi.org/10.1017/S0022377818000041>`_

N. R. Mandell, W. D. Dorland, I. Abel, R. Gaur, P. Kim, M. Martin, and T. Qian. 2024. "GX: a GPU-native gyrokinetic turbulence code for tokamak and stellarator design". J. Plasma Phys. **90**, 905900402. `<https://doi.org/10.1017/S0022377824000631>`_
