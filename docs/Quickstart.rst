.. _quickstart:

Getting started with GX
+++++++++++++++++++++++

.. toctree::
   :maxdepth: 2

   Linear
   LinearStell
   Nonlinear
   Benchmarks
   MultiGPU
