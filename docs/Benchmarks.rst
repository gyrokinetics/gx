.. _quickbench:

Running benchmarks
++++++++++++++++++

A number of additional example input files are included in the GX repo in the ``benchmarks`` `directory <https://bitbucket.org/gyrokinetics/gx/src/gx/benchmarks/>`_. In most cases, a ``check.py`` script is provided to check the results, with usage instructions provided in the ``README`` for each case.
