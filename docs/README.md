This is the documentation for the GX code. To generate an html version, use

$ make html

which will build the html pages in `_build/html`. The top level page is `_build/html/index.html`.

This requires python with sphinx.
