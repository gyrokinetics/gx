# This test runs a nonlinear collisional ETG (slab) case. 
# This model uses a Boltzmann adiabatic ion response.

 debug = false

[Dimensions]
 ntheta = 48             # number of points along field line
 ny = 128                # number of real-space grid-points in y
 nx = 128                # number of real-space grid-points in x
 
[Domain]
 x0 = 6.366
 y0 = 6.366
 z0 = 3.183
 boundary = "periodic"   

[Collisional_slab_ETG]
 cetg = true

[Physics]
 nonlinear_mode = true           # this is a nonlinear calculation

[Time]
 t_max = 500.0        # run to t = t_max (units of L_ref/vt_ref)
 dt = 0.005            # maximum timestep (units of L_ref/vt_ref), will be adapted dynamically
 cfl = 1.0            # safety cushion factor on timestep size
 scheme = "sspx3"       # use SSPx3 timestepping scheme

[Initialization]
 ikpar_init = 1                  # parallel wavenumber of initial perturbation
 init_field = "density"          # initial condition set in density
 init_amp = 1.0e-2               # amplitude of initial condition

[Geometry]
 zero_shat = true
 geo_option = "slab"

[Boltzmann]
 add_Boltzmann_species = true    # use a Boltzmann species
 Boltzmann_type = "ions"         # the Boltzmann species will be ions
 tau_fac = 1.0                   # generalized temperature ratio, T_i/(Z T_e)
 Z_ion = 1.0                     # ion charge

[Dissipation]
 hyper = true                    # use hyperdiffusion
 D_hyper = 5.0e-4                # coefficient of hyperdiffusion

[Restart]
 restart = false
 save_for_restart = true

[Diagnostics]
 nwrite = 500                    # write diagnostics every nwrite timesteps
 fluxes = true                   # compute and write heat and particle fluxes
 omega  = false
 
[Wspectra]                       # spectra of W = |G_lm|**2
 laguerre         = true
 kx               = true
 ky               = true          # W(ky) (summed over kx, z, l, m)
 kxky             = true
 z                = true

[Qspectra]                      # spectra of Q (heat flux)
 kx               = true
 ky               = true         # Q(ky) (summed over kx, z)
 kxky             = true
 z                = true         # Q(z) (summed over kx, ky)

[Expert]

 dealias_kz       = true
