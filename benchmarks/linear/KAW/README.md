This test runs a linear kinetic Alfven wave calculation in slab geometry.

To run the test, simply use 
```
[/path/to/]gx kaw_betahat10.0_kp0.01.in
```

To check the results, use
```
python check.py kaw_betahat10.0_kp0.01
```

